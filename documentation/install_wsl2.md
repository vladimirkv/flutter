# Установка на Windows (WSL2)

Работа Flutter SDK была проверена на [Windows Subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/about). Установка Flutter SDK с поддержкой ОС Аврора аналогична установке описанной в документации для Linux - [Установка на Linux](install_linux.md). Сборка приложений Flutter будет осуществляться с помощью CLI Flutter - детальное описание команд можно найти в документации - "[Flutter CLI на ОС Аврора](cli.md)".

> Для работы Flutter SDK требуется **вторая** версия WSL! Перед установкой ознакомитесь с документацией по обновлению версии WSL - "[Upgrade version from WSL 1 to WSL 2](https://learn.microsoft.com/en-us/windows/wsl/install#upgrade-version-from-wsl-1-to-wsl-2)".
